<?php
if(!function_exists('custom_pdf_attachment_shortcode')){
	function custom_pdf_attachment_shortcode( $atts ) {
		 global $post;
		 extract( shortcode_atts( array(
			  'file' => '',
			  'name' => ''
		 ), $atts ) );
		 
		 if(!$file){
			return;
		 }
		 
		 if(!cpta_get_file_url($post->ID,$file)){
			return;
		 }
		 
		 $pdf_open_in = get_option('pdf_open_in');
		 $target = ( $pdf_open_in == '' ? '_self' : $pdf_open_in ); 
		 
		 if($name){
			$ret = '<img src="'.plugins_url( CPTA_PLUGIN_DIR . '/images/pdf.png' ).'"> <a href="'.cpta_get_file_url($post->ID,$file).'" target="'.$target.'">'.$name.'</a>';
		} else {
			$file_mime_data = wp_check_filetype(cpta_get_file_url($post->ID,$file));
			$file_info = pathinfo(cpta_get_file_url($post->ID,$file));
			$ret = '<img src="'.plugins_url( CPTA_PLUGIN_DIR . '/images/pdf.png' ).'"> <a href="'.cpta_get_file_url($post->ID,$file).'" target="'.$target.'">'.$file_info['basename'].'</a>';
		}
		 return $ret;
	}
}

if(!function_exists('pdf_attachment_file')){
	function pdf_attachment_file($file = '', $name = ''){
		if(!$file){
			return;
		}
		return do_shortcode('[pdf_attachment file="'.$file.'" name="'.$name.'"]');
	}
}

if(!function_exists('custom_pdf_all_attachments_shortcode')){
	function custom_pdf_all_attachments_shortcode() {
		 global $post;
		 $saved_no_of_pdf_attachment = (int)get_option('saved_no_of_pdf_attachment');
		 $ret = '';
		 for($i=1; $i <= $saved_no_of_pdf_attachment; $i++ ){
			 $file_att = pdf_attachment_file($i);
			 if($file_att){
				$ret .= '<p>';
				$ret .= $file_att;
				$ret .= '</p>';
			 }
		 }
		 return $ret;
	}
}

if(!function_exists('pdf_all_attachment_files')){
	function pdf_all_attachment_files(){
		return do_shortcode('[pdf_all_attachments]');
	}
}


=== Newsletter Addon for BuddyPress ===
Tags: newsletter, subscription, buddypress, registration
Requires at least: 3.4.0
Tested up to: 5.2.3
Stable tag: 1.0.2
Requires PHP: 5.6
Contributors: satollo,webagile,michael-travan

Integrates the BuddyPress registration with the Newsletter subscription

== Description ==

With this Newsletter Addon, you can integrate the BuddyPress registration process with
Newsletter subscription, offering an option on the registration form or just subscribing
all new registered BuddyPress members.

You can even set one or more list to activate on subscribers when they are created from a
BuddyPress registration.

**Every contribution (idea, patch, bug report, ...) is greatly welcomed.**

= References =

* [The Newsletter Plugin](https://wordpress.org/plugins/newsletter)
* [BuddyPress](https://wordpress.org/plugins/buddypress/)

== Frequently Asked Questions ==

= Where can I find support or send a suggest? =

Please, use the [support forum](https://wordpress.org/support/plugin/newsletter-buddypress/).

== Changelog ==

= 1.0.2 =

* Fixed activation code (possible fatal error)

= 1.0.1 =

* Cleaned up obsolete code
* Fixed text domain declaration

= 1.0.0 =

* First public release
